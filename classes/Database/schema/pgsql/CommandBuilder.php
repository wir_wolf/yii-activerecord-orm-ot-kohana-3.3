<?php
namespace Database\schema\pgsql;
use Database\schema as BaseSchema;
/**
 * CommandBuilder class file.
 *
 * @author Timur Ruziev <resurtm@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

/**
 * CommandBuilder provides basic methods to create query commands for tables.
 *
 * @author Timur Ruziev <resurtm@gmail.com>
 * @package system.db.schema.pgsql
 * @since 1.1.14
 */
class CommandBuilder extends BaseSchema\CommandBuilder
{
	/**
	 * Returns default value of the integer/serial primary key. Default value means that the next
	 * autoincrement/sequence value would be used.
	 * @return string default value of the integer/serial primary key.
	 * @since 1.1.14
	 */
	protected function getIntegerPrimaryKeyDefaultValue()
	{
		return 'DEFAULT';
	}
}
